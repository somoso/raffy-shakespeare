// Code-splitting is automated for `routes` directory
import Raff from "../routes/raff-insulter";

const App = () => (
	<Raff />
)

export default App;
